Firmware for MUR supervisor. Use STM8S003 chip.

## Functions ##
* Vehicle power management
* Manipulate button
* Manipulate LED
* Handle ADC data (depth, temperature, battery)
* UART communication
* Leak alert system