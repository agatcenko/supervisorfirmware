#include "stm8s.h"
#include "stm8s_it.h"

#include "led_utils.h"
#include "uart_utils.h"
#include "button_utils.h"
#include "sensor_utils.h"

void initClock();
void initSystem();

int main( void )
{
  initClock(); // Initialize external clock
  initSystem(); // Initialize all system
  
  enableInterrupts();
  
  while (1) {
  
  }
  
  return 0;
}

void initClock()
{
  CLK_DeInit();
  
  // Work on 16MHz, external clock
  u8 cnt = 0;
  CLK_HSECmd(ENABLE);
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSE, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE); 
  while (CLK_GetSYSCLKSource() != CLK_SOURCE_HSE) { // Wait for switch
    if (--cnt == 0) {
      return;
    }
  }  
}

void initSystem()
{
  initAdc(); // Initialize analog-to-digital converter
  initLed(); // Initialize light-emitting diode
  initButton(); // Initialize button
  initConnection(); // Initialize rs-485 connection
  initWaterSensor(); // Initialize water sensor gpio pin
}