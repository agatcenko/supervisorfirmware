#ifndef LED_UTILS_H
#define LED_UTILS_H

#include "stm8s.h"

// Red LED gpio pin
#define RED_LED_PORT GPIOC
#define RED_LED_PIN GPIO_PIN_6

// Green LED gpio pin
#define GREEN_LED_PORT GPIOC
#define GREEN_LED_PIN GPIO_PIN_7

// Enum with LED colors
typedef enum LedColor_x
{
  UNDEF = 0x00,
  RED = 0x01,
  GREEN = 0x02,
} LedColor;

// Enum with LED status
typedef enum LedStatus_x
{
  ON = 0x00,
  OFF = 0x01
} LedStatus;

// Enum with LED mode
typedef enum LedMode_x
{
  WAIT = 0x00,      // wait mode - default
  UPLOAD = 0x01,    // upload binary
  EXECUTE = 0x02,   // execute binary
  POWER_OFF = 0x03, // power off mode
  ALERT = 0x04      // indicate leak
} LedMode;

void initLed(); // Initialize LED
void turnLedOn(LedColor color); // Turn on LED with color 
void turnLedOff(LedColor color); // Turn off LED with color

void powerOffLeds(); // Turn off LED
void switchCurrentLed(); // Change LED status (ON/OFF)
void switchCurrentLedAndColor(); // Change LED status and color

void checkLeakStatus(uint8_t leak);
void changeBatteryIndication(LedColor color);
void checkBatteryIndication(uint8_t percents);

LedMode getLedMode();
void setLedMode(LedMode mode);

LedStatus getLedStatus();
void handleLedStatus();

#endif