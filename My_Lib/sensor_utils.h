#ifndef SENSOR_UTILS_H
#define SENSOR_UTILS_H

#include "stm8s.h"

// WS - water sensor pin
#define WS_PORT GPIOA
#define WS_PIN GPIO_PIN_3

// Size of adc samples buffer for filtering
#define ADC_SAMPLE_SIZE 3

// Structure for storing device info
typedef struct SensorsData {
  uint16_t batteryV;    // raw battery voltage
  uint16_t temperature; // raw temperature
  uint16_t depth;       // raw depth
  uint8_t leak;         // leak status (1 - leak, 0 - no leak)
} SensorsData;

void initAdc(); // Initialize analog-to-digital converter
void initWaterSensor(); // Initialize water sensor gpio

uint8_t checkLeak(); // Read leak gpio pin
void sortSwap(uint16_t* arr, uint8_t i, uint8_t j); // Sort two elements (increase)
void filterRawAdcValues(); // Choose median value from array of adc raw data
void readSensorValue(uint16_t* value, ADC1_Channel_TypeDef channel); // Read raw adc data from adc channel

void readSensorValues(); // Read battery volts, temperature, depth
SensorsData getSensorsData(); // Return current device info structure

float batteryCalibrate(uint16_t x); // Calculate real value of battery in volts
uint8_t batteryPercents(float currentVolts); // calculate remaining percents of battery capasity

#endif