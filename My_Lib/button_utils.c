#include "led_utils.h"
#include "button_utils.h"

BitStatus prevButtonStatus = RESET, curButtonStatus = RESET;
uint8_t timePressed = 1; 
uint8_t powerState = 0;
uint8_t buttonStatus = 0;

void initButton()
{
  GPIO_Init(BUTTON_PORT, BUTTON_PIN, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(POWER_PORT, POWER_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_WriteLow(POWER_PORT, POWER_PIN); // At start vehicle is off
}

uint8_t getPressedTime()
{
  prevButtonStatus = curButtonStatus;
  curButtonStatus = GPIO_ReadInputPin(BUTTON_PORT, BUTTON_PIN);
  
  if(!prevButtonStatus && !curButtonStatus) { // button is pressed
    ++timePressed; // increment button pressed time
    if(timePressed >= SWITCH_POWER_TIME) { // Time is enough for switch power
      uint8_t tmp = timePressed;
      timePressed = SWITCH_POWER_TIME + 1;
      return tmp;
    }
  }
  
  if(!prevButtonStatus && curButtonStatus) { // Button was released
    uint8_t tmp = timePressed;
    timePressed = 1;
    return tmp;
  }
  
  return 0;
}

void turnPowerOn()
{
  if (getLedMode() != ALERT) {
    GPIO_WriteHigh(POWER_PORT, POWER_PIN);
    powerState = 1;
    setLedMode(WAIT);
  }
}

void turnPowerOff()
{
  GPIO_WriteLow(POWER_PORT, POWER_PIN);
  powerState = 0;
  if (getLedMode() != ALERT) {
    setLedMode(POWER_OFF);
  }
}

void switchPower()
{
  if (powerState == 0) {
    turnPowerOn();
  }
  else {
    turnPowerOff();
  }
}

uint8_t getPowerState()
{
  return powerState;
}

uint8_t getButtonStatus()
{
  return buttonStatus;
}

void setButtonStatus(uint8_t status)
{
  buttonStatus = status;
}