#ifndef BUTTON_UTILS_H
#define BUTTON_UTILS_H

#include "stm8s.h"

#define SWITCH_POWER_TIME 15 // Time for button pressed (in tacts, over 3 seconds) needed to switch vehicle power 

// GPIO pin for button
#define BUTTON_PORT GPIOB
#define BUTTON_PIN GPIO_PIN_5
// GPIO pin for power management
#define POWER_PORT GPIOC
#define POWER_PIN GPIO_PIN_3

void initButton(); // Initialize button

void turnPowerOn();  // Power on vehicle
void turnPowerOff(); // Power off vehicle
void switchPower();  // Switch power (ON/OFF)

uint8_t getPowerState();
uint8_t getPressedTime(); // Get pressed button time

uint8_t getButtonStatus();
void setButtonStatus(uint8_t status);

#endif