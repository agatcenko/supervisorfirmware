#include "stm8s.h"
#include "stdlib.h"
#include "led_utils.h"
#include "uart_utils.h"
#include "button_utils.h"
#include "sensor_utils.h"

// for crc16 calculation
#define POLY 0x1021 // CRC16 polynominal representation
#define SEED 0xFFFF // Init value in CRC16 algo

#define BAUDRATE 115200 // Baudrate value

// Devices
#define MAIN_DEVICE_ID 250 // Id of edison computer

// fields in received buffer
#define TRANSMITTER_ID 3 // Location of package transmitter id byte
#define RECEIVER_ID 4    // Location of package receiver id byte
#define REPLY_MODE 5     // Location of package confirmation flag
#define MESSAGE_ID 6     // Location of package id byte

// reply modes
#define WITHOUT_REPLY 0x00 // No need to answer on message
#define WITH_REPLY 0x0A    // Need to answer on message
#define IS_REPLY 0x0B      // This message is an answer

// packages id
#define ERROR_MSG_ID 0xFF            // Id for Error package
#define SET_DEVICE_ADDRESS 0x01      // Id for SetDeviceAddress package
#define REQUEST_DEVICE_ADDRESS 0x02  // Id for RequestDeviceAddress package
#define REPLY_DEVICE_ADDRESS 0x03    // Id for ReplyDeviceAddress package
#define REQUEST_DEVICE_TYPE 0x04     // Id for RequestDeviceType package
#define REPLY_DEVICE_TYPE 0x05       // Id for ReplyDeviceType package
#define REQUEST_DATA_MSG_ID 0x0B     // Id for RequestData package
#define REPLY_DATA_MSG_ID 0x0C       // Id for ReplyData package
#define CONTROL_LED 0x0D             // Id for ConrolLed package

// output packages size 
#define ERROR_MSG_SIZE 0x0A          // Size for Error package
#define DEVICE_TYPE_MSG_SIZE 0x0B    // Size for ReplyDeviceType package
#define DEVICE_ADDRESS_MSG_SIZE 0x0B // Size for ReplyDeviceAddress package
#define DATA_REPLY_MSG_SIZE 0x11     // Size for ReplyData package

uint8_t firstPreamble = 0; // flag, was received byte 0x7D  
uint8_t readSizeByte = 0;  // flag, need to read size
uint8_t inPackage = 0;     // flag, put new byte in package
uint8_t currentPackageSize = 0; // size of current package
uint8_t currentPackageCounter = 0;
uint8_t receivedBuffer[U8_MAX + 1];
uint8_t transmitBuffer[U8_MAX + 1];

uint8_t thisDeviceType = 0; // 0 - Supervisor
uint8_t thisDeviceAddress = 100; // default address

// Passive wait function
static void Delay(uint16_t nCount)
{
  // Decrement nCount value
  while (nCount != 0) {
    nCount--;
  }
}

void initConnection()
{
  UART1_DeInit();
  UART1_Init((uint32_t)BAUDRATE, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  GPIO_Init(DIR_PORT, DIR_PIN, GPIO_MODE_OUT_PP_LOW_FAST); //receive, transmit mode pin
  activateReceive(); // Activate receive mode
  ITC_SetSoftwarePriority(ITC_IRQ_UART1_RX, ITC_PRIORITYLEVEL_3);
  UART1_ITConfig(UART1_IT_RXNE, ENABLE); // Enable interrupt in receive
  UART1_Cmd(ENABLE);
}

uint8_t getDeviceType()
{
  return thisDeviceType;
}

uint8_t getDeviceAddress()
{
  return thisDeviceAddress;
}

void setDeviceAddress(uint8_t address)
{
  thisDeviceAddress = address;
  FLASH_ProgramByte(0x4000, thisDeviceAddress);
}

void activateReceive()
{
  GPIO_WriteLow(DIR_PORT, DIR_PIN);
}

void activateTransmit()
{
  GPIO_WriteHigh(DIR_PORT, DIR_PIN);
}

void transmitData(uint8_t *data, uint8_t len) {
  activateTransmit();
  
  // Calculate crc16
  if (len > 2) {
    uint16_t crc = calculateCRC(data, len - 3); // -3 because of two crc bytes and ended 0x7E
    data[len - 3] = crc >> 8;
    data[len - 2] = crc & 0xFF;
  }
  
  while (len--) {
    UART1_SendData8(*data++);
    while(UART1_GetFlagStatus(UART1_FLAG_TC) == RESET);
  }
  
  activateReceive();
}

void handleReceivedByte(uint8_t byte)
{
  if (firstPreamble) {
     if (byte == 0x7F && !inPackage) { // Looking for second preamble byte
       inPackage = 1; // Analyze package body
       readSizeByte = 1; // Next byte will be with package size
     }
     firstPreamble = 0; // It was not a preamble
     return;
  }

  if (byte == 0x7D && !inPackage) { // Looking for start of package, first preamble byte = 0x7D
     firstPreamble = 1; // Found first preamble byte
     return;
  }

  if (readSizeByte) {
     readSizeByte = 0;
     if (byte < 0x0A) { // this is the smallest size of package
       inPackage = 0;
       return;
     }
     
     currentPackageSize = byte;
     receivedBuffer[0] = 0x7D;
     receivedBuffer[1] = 0x7F;
     receivedBuffer[2] = byte;
     currentPackageCounter = 3;

     return;
  }
  
  if (inPackage && !readSizeByte) {
     receivedBuffer[currentPackageCounter] = byte;
     currentPackageCounter++;
     if (currentPackageCounter == currentPackageSize) {
       if (byte == 0x7E) { // Check last byte of package
         processReceivedBuffer(); // Analyze package
       }
       inPackage = 0;
     }
   }
}

void processReceivedBuffer()
{
  // Compare transmitter id
  if (receivedBuffer[TRANSMITTER_ID] != MAIN_DEVICE_ID) {
    return;
  }
  
  // Compare receiver id
  if (receivedBuffer[RECEIVER_ID] != getDeviceAddress()) {
    return;
  }
  
  // Compare crc
  uint16_t receivedCrc = (receivedBuffer[currentPackageSize - 3] << 8) + receivedBuffer[currentPackageSize - 2];
  uint16_t calculatedCrc = calculateCRC(receivedBuffer, currentPackageSize - 3);
  if (receivedCrc != calculatedCrc) {
    transmitErrorMsg(); // maybe don`t need
    return;
  }
  
  // Compare msg id
  switch (receivedBuffer[MESSAGE_ID]) {
    case REQUEST_DATA_MSG_ID:
      if (receivedBuffer[MESSAGE_ID + 1] != 0) { // Check status of button process on edison
        setButtonStatus(0); // If button was processed - clear status
      }
      transmitDataMsg(); // Send data info 
      break;
    
    case CONTROL_LED:
      if (receivedBuffer[MESSAGE_ID + 1] == 0x0A) {
        setLedMode(UPLOAD); // Set upload binary mode
      }
      else if (receivedBuffer[MESSAGE_ID + 1] == 0x14) {
        setLedMode(EXECUTE); // Set execute binary mode
      }
      else if (receivedBuffer[MESSAGE_ID + 1] == 0x1E) {
        setLedMode(WAIT); // Set wait mode for led
      }
      break;
    
    case REQUEST_DEVICE_ADDRESS:
      transmitAddressMsg();
      break;
    
    case SET_DEVICE_ADDRESS:
      setDeviceAddress(receivedBuffer[MESSAGE_ID + 1]);
      break;
    
    case REQUEST_DEVICE_TYPE:
      transmitTypeMsg();
      break;
      
    default:
      if (receivedBuffer[REPLY_MODE] == WITH_REPLY) {
        transmitErrorMsg();
      }
      break;
  }
}

void prepareTransmitBuffer(uint8_t size)
{ 
  transmitBuffer[0] = 0x7D;
  transmitBuffer[1] = 0x7F;
  transmitBuffer[2] = size;
  transmitBuffer[TRANSMITTER_ID] = getDeviceAddress();
  transmitBuffer[RECEIVER_ID] = MAIN_DEVICE_ID;
  transmitBuffer[REPLY_MODE] = IS_REPLY;
  transmitBuffer[size - 1] = 0x7E;
}

void transmitDataMsg()
{ 
  prepareTransmitBuffer(DATA_REPLY_MSG_SIZE);
  transmitBuffer[MESSAGE_ID] = REPLY_DATA_MSG_ID;
  
  SensorsData data = getSensorsData();
  uint8_t buttonStatus = getButtonStatus();
  transmitBuffer[MESSAGE_ID + 1] = data.batteryV >> 8;
  transmitBuffer[MESSAGE_ID + 2] = data.batteryV & 0xFF;
  transmitBuffer[MESSAGE_ID + 3] = data.depth >> 8;
  transmitBuffer[MESSAGE_ID + 4] = data.depth & 0xFF;
  transmitBuffer[MESSAGE_ID + 5] = data.temperature >> 8;
  transmitBuffer[MESSAGE_ID + 6] = data.temperature & 0xFF;
  uint8_t buttonAndLeak = 0x00; // no leak, no button
  if (data.leak && buttonStatus) {
    buttonAndLeak = 0xAA;
  }
  else if (data.leak && !buttonStatus) {
    buttonAndLeak = 0xA0;
  }
  else if (!data.leak && buttonStatus) {
    buttonAndLeak = 0x0A;
  }
  transmitBuffer[MESSAGE_ID + 7] = buttonAndLeak;
  
  transmitData(transmitBuffer, DATA_REPLY_MSG_SIZE);
}

void transmitErrorMsg()
{ 
  prepareTransmitBuffer(ERROR_MSG_SIZE);
  transmitBuffer[MESSAGE_ID] = ERROR_MSG_ID;
  transmitData(transmitBuffer, ERROR_MSG_SIZE);
}

void transmitTypeMsg()
{
  prepareTransmitBuffer(DEVICE_TYPE_MSG_SIZE);
  transmitBuffer[MESSAGE_ID] = REPLY_DEVICE_TYPE;
  transmitBuffer[MESSAGE_ID + 1] = getDeviceType();
  transmitData(transmitBuffer, DEVICE_TYPE_MSG_SIZE);
}

void transmitAddressMsg()
{
  prepareTransmitBuffer(DEVICE_ADDRESS_MSG_SIZE);
  transmitBuffer[MESSAGE_ID] = REPLY_DEVICE_ADDRESS;
  transmitBuffer[MESSAGE_ID + 1] = getDeviceAddress();
  transmitData(transmitBuffer, DEVICE_ADDRESS_MSG_SIZE);
}

uint16_t updateCRC(uint16_t acc, const uint8_t input)
{
    // Create the CRC "dividend" for polynomial arithmetic (binary arithmetic
    // with no carries)
    acc ^= (input << 8);

    // "Divide" the poly into the dividend using CRC XOR subtraction
    // CRC_acc holds the "remainder" of each divide
    // Only complete this division for 8 bits since input is 1 byte

    for (uint8_t i = 0; i < 8; i++)
    {
        // Check if the MSB is set (if MSB is 1, then the POLY can "divide"
        // into the "dividend")

        if ((acc & 0x8000) == 0x8000) {
            acc <<= 1;
            acc ^= POLY;
        }
        else {
            acc <<= 1;
        }
    }

    return acc;
}

uint16_t calculateCRC(const uint8_t *data, const int32_t len)
{
    uint16_t crcout = SEED;

    for (int32_t i = 0; i < len; i++) {
        crcout = updateCRC(crcout, data[i]);
    }

    return crcout;
}