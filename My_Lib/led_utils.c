#include "stm8s.h"
#include "led_utils.h"
#include "button_utils.h"
#include "sensor_utils.h"

#define ledTimerFrequency 6 // Timer freaqunce for LED update = 6 Hz
#define executeModeFrequency (ledTimerFrequency / 2) // LED blink frequency for execute mode = 2 Hz
#define uploadModeFrequency (ledTimerFrequency / 3)  // LED blink frequency for upload mode = 3 Hz
#define alertModeFrequency (ledTimerFrequency / 6)   // Led blink frequency for alert mode = 6 Hz

LedMode currentLedMode;
LedColor currentLedColor;
LedStatus currentLedStatus;

const uint8_t LEAK_VALUE = 0;

// Percent levels for battery
const uint8_t CRITICAL_LEVEL = 0x1E; // Turn on RED color when battery voltage <= 30%

uint8_t ledTimerCounter = 0;

void initLed()
{
  // Initialize GPIO pins
  GPIO_Init(RED_LED_PORT, RED_LED_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GREEN_LED_PORT, GREEN_LED_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  
  // Activate timer for battery check
  TIM1_TimeBaseInit(32768, TIM1_COUNTERMODE_UP, 0x0051, 0); // 6Hz
  TIM1_Cmd(ENABLE);
  ITC_SetSoftwarePriority(ITC_IRQ_TIM1_OVF, ITC_PRIORITYLEVEL_1); // Set low priority for interrupt
  TIM1_ITConfig(TIM1_IT_UPDATE, ENABLE); // Enable interrupt
  
  // Initialize settings
  currentLedMode = POWER_OFF;
  currentLedStatus = OFF;
  currentLedColor = UNDEF;
  
  turnLedOff(RED);
  turnLedOff(GREEN);
}

void turnLedOn(LedColor color)
{
  if (color == RED) {
    GPIO_WriteHigh(RED_LED_PORT, RED_LED_PIN);
  }
  else if (color == GREEN) {
    GPIO_WriteHigh(GREEN_LED_PORT, GREEN_LED_PIN);
  }
  else {
    return;
  }
  
  currentLedStatus = ON;
}

void turnLedOff(LedColor color)
{
  if (color == RED) {
    GPIO_WriteLow(RED_LED_PORT, RED_LED_PIN);
  }
  else if (color == GREEN) {
    GPIO_WriteLow(GREEN_LED_PORT, GREEN_LED_PIN);
  }
  else {
    return;
  }
  
  currentLedStatus = OFF;
}

void switchCurrentLed()
{
  if (currentLedStatus == ON) {
    turnLedOff(currentLedColor);
  }
  else {
    turnLedOn(currentLedColor);
  }
}

void switchCurrentLedAndColor()
{
  if (currentLedStatus == ON) {
    turnLedOff(currentLedColor);
  }
  else {
    if (currentLedColor == RED || currentLedColor == UNDEF) {
      currentLedColor = GREEN;
    }
    else {
      currentLedColor = RED;
    }
    turnLedOn(currentLedColor);
  }
}

void powerOffLeds()
{
  if (currentLedStatus == ON) {
    turnLedOff(currentLedColor);
    currentLedColor = UNDEF;
  }
}

void checkLeakStatus(uint8_t leak)
{
  if (leak == LEAK_VALUE && currentLedMode != ALERT) {
    setLedMode(ALERT);
    turnPowerOff(); // Turn off vehicle if there is a leak!!!
  }
  else if (leak != LEAK_VALUE && currentLedMode == ALERT) {
    setLedMode(POWER_OFF);
  }
}

void changeBatteryIndication(LedColor color)
{
  if (currentLedMode != POWER_OFF && currentLedMode != ALERT) {
    turnLedOff(currentLedColor);
    currentLedColor = color;
    turnLedOn(color);
  }
}

void checkBatteryIndication(uint8_t percents)
{
  if (percents >= CRITICAL_LEVEL && currentLedColor != GREEN) {
    changeBatteryIndication(GREEN); // Use GREEN LED if battery voltage is more or equal to critical level 
  }
  else if (percents < CRITICAL_LEVEL && currentLedColor != RED) {
    changeBatteryIndication(RED); // Use RED LED if battery voltage is less than critical level 
  }
}

LedMode getLedMode()
{
  return currentLedMode;
}

void setLedMode(LedMode mode)
{
  currentLedMode = mode;
}

LedStatus getLedStatus()
{
  return currentLedStatus;
}

void handleLedStatus()
{
  ledTimerCounter++;
  if (ledTimerCounter >= ledTimerFrequency) {
    ledTimerCounter = 0; // New period
    SensorsData data = getSensorsData();
    float currentVolts = batteryCalibrate(data.batteryV);
    uint8_t percents = batteryPercents(currentVolts); // get percents for battery
    checkBatteryIndication(percents);
    checkLeakStatus(data.leak);
  }
    
  LedMode currentMode = getLedMode();
  // Look for modes
  if ((ledTimerCounter % executeModeFrequency) == 0 && currentMode == EXECUTE) {
    switchCurrentLed();
  }
  else if ((ledTimerCounter % uploadModeFrequency) == 0 && currentMode == UPLOAD) {
    switchCurrentLed();
  }
  else if ((ledTimerCounter % alertModeFrequency) == 0 && currentMode == ALERT) {
    switchCurrentLedAndColor();
  }
  else if (currentMode == POWER_OFF) {
    powerOffLeds();
  }
  else if (currentMode == WAIT) {
    if (getLedStatus() == OFF) {
      switchCurrentLed();
    }
  }
}