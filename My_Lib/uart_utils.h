#ifndef UART_UTILS_H
#define UART_UTILS_H

// RS-485 direction change pin
#define DIR_PORT GPIOD
#define DIR_PIN GPIO_PIN_4

void initConnection(); // Initialize uart1

uint8_t getDeviceType(); // Get this device type
uint8_t getDeviceAddress(); // Get this device address
void setDeviceAddress(uint8_t address); // Set new address for this device

void activateReceive(); // Activate receive mode for rs-485
void activateTransmit(); // Activate transmit mode for rs-485

void transmitData(uint8_t *data, uint8_t len); // Transmit buffer data over uart

void handleReceivedByte(uint8_t byte); // Anakyze next byte from uart input
void processReceivedBuffer(); // Decode input data

void prepareTransmitBuffer(uint8_t size); // Set service info for output package
void transmitDataMsg(); // Transmit msg with device info
void transmitErrorMsg(); // Transmit msg with error
void transmitTypeMsg(); // Transmit msg with this device type
void transmitAddressMsg(); // Transmit msg with this device address

// Functions for CRC16 calculation
uint16_t updateCRC(uint16_t acc, const uint8_t input);
uint16_t calculateCRC(const uint8_t *data, const int32_t len);

#endif