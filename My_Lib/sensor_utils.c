#include "sensor_utils.h"

// Constants for convertation battery adc values to volts
// Use linear interpolation
const uint16_t batteryAdc1 = 388; // First point (raw adc data) 
const uint16_t batteryAdc2 = 502; // Second point (raw adc data)
const float batteryVolts1 = 12.0f; // First point (real data in volts)
const float batteryVolts2 = 15.5f; // Second point (real data in volts)
// Constants for min and max battery voltage
const float minBatteryVolts = 12.0f; // Minimum value in volts for battery (equal to 0%)
const float maxBatteryVolts = 16.8f; // Maximum value in volts for battery (equal to 100%) 

SensorsData sensorsData;
uint8_t sampleCounter = 0;

uint16_t rawBatteryV[ADC_SAMPLE_SIZE];    // Buffer for raw battery voltage data
uint16_t rawTemperature[ADC_SAMPLE_SIZE]; // Buffer for raw temperature data
uint16_t rawDepth[ADC_SAMPLE_SIZE];       // Buffer for raw depth data

// Passive wait function
static void Delay(uint16_t nCount)
{
  /* Decrement nCount value */
  while (nCount != 0) {
    nCount--;
  }
}

void initAdc()
{
  TIM2_TimeBaseInit(TIM2_PRESCALER_128, 0x0FFF); // Timer frequency = 30 Hz
  TIM2_Cmd(ENABLE);
  ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_2, ADC1_PRESSEL_FCPU_D2, ADC1_EXTTRIG_TIM, DISABLE, ADC1_ALIGN_RIGHT, ADC1_SCHMITTTRIG_CHANNEL2, DISABLE); // chanel for battery volts
  ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_3, ADC1_PRESSEL_FCPU_D2, ADC1_EXTTRIG_TIM, DISABLE, ADC1_ALIGN_RIGHT, ADC1_SCHMITTTRIG_CHANNEL3, DISABLE); // channel for temperature
  ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_4, ADC1_PRESSEL_FCPU_D2, ADC1_EXTTRIG_TIM, DISABLE, ADC1_ALIGN_RIGHT, ADC1_SCHMITTTRIG_CHANNEL4, DISABLE); // channel for depth
  ITC_SetSoftwarePriority(ITC_IRQ_TIM2_OVF, ITC_PRIORITYLEVEL_1); // Low priority of interrupts
  TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE); // Enable interrupt
  
  sampleCounter = 0;
}

void initWaterSensor()
{
  GPIO_Init(WS_PORT, WS_PIN, GPIO_MODE_IN_PU_NO_IT);
}

uint8_t checkLeak()
{
  return GPIO_ReadInputPin(WS_PORT, WS_PIN);
}

void sortSwap(uint16_t* arr, uint8_t i, uint8_t j)
{
  if (arr[i] > arr[j]) {
    uint16_t temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }
}

void filterRawAdcValues()
{
  // Simple bubble sort
  for (uint8_t i = 0; i < ADC_SAMPLE_SIZE - 1; ++i) {
    for (uint8_t j = 0; j < ADC_SAMPLE_SIZE - 1; ++j) {
      sortSwap(rawBatteryV, j, j + 1);
      sortSwap(rawTemperature, j, j + 1);
      sortSwap(rawDepth, j, j + 1);
    }
  }
  
  // Get median values
  uint8_t mid = ADC_SAMPLE_SIZE / 2;
  sensorsData.batteryV = rawBatteryV[mid];
  sensorsData.temperature = rawTemperature[mid];
  sensorsData.depth = rawDepth[mid];
  sensorsData.leak = checkLeak();
}

void readSensorValue(uint16_t* value, ADC1_Channel_TypeDef channel)
{
  ADC1_ConversionConfig(ADC1_CONVERSIONMODE_SINGLE, channel, ADC1_ALIGN_RIGHT);
  Delay(5);
  ADC1_StartConversion();
  Delay(10);
  *value = ADC1_GetConversionValue();
}

void readSensorValues()
{
  if (sampleCounter >= ADC_SAMPLE_SIZE) {
    sampleCounter = 0;
  }
  
  readSensorValue(&rawBatteryV[sampleCounter], ADC1_CHANNEL_2);
  readSensorValue(&rawTemperature[sampleCounter], ADC1_CHANNEL_3);
  readSensorValue(&rawDepth[sampleCounter], ADC1_CHANNEL_4);

  sampleCounter += 1;
  
  if (sampleCounter == ADC_SAMPLE_SIZE) { // Collect needed values number
    sampleCounter = 0;
    filterRawAdcValues();
  }
}

SensorsData getSensorsData()
{
  return sensorsData;
}

float batteryCalibrate(uint16_t x)
{
  // linear function, get battery value in voltage from raw adc data
  return batteryVolts1 + ((float)x - batteryAdc1) * (batteryVolts2 - batteryVolts1) / ((float)batteryAdc2 - (float)batteryAdc1);
}

uint8_t batteryPercents(float currentVolts)
{
  if (currentVolts >= maxBatteryVolts) {
    return 100; // up bound
  }
  if (currentVolts <= minBatteryVolts) {
    return 0; // low bound
  }
  
  // Get persents for battery
  return (currentVolts - minBatteryVolts) / (maxBatteryVolts - minBatteryVolts) * 100;
}
